
import { baseUrl } from './config.js';

export function topList(){
	return uni.request({
			url: `${baseUrl}/toplist/detail`,
			method: 'GET'
		})
}

export function listDetail(listId)
{
	return uni.request({
		url:`${baseUrl}/playlist/detail?id=${listId}`,
		method:'GET'
	})
}

export function songDetail(id){
	return uni.request({
		url : `${baseUrl}/song/detail?ids=${id}`,
		method : 'GET'
	})
}

export function songUrl(id){
	return uni.request({
		url : `${baseUrl}/song/url?id=${id}`,
		method : 'GET'
	})
}

export function songLyric(id){
	return uni.request({
		url : `${baseUrl}/lyric?id=${id}`,
		method : 'GET'
	})
}

export async function uniCloudDemo()
{
	


}